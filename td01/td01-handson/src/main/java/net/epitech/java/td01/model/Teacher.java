package net.epitech.java.td01.model;

public class Teacher
{
	public Integer getId() {
		return _id;
	}
	public void setId(Integer id) {
		this._id = id;
	}
	public String getName() {
		return _name;
	}
	public void setName(String name) {
		this._name = name;
	}
	public String getMail() {
		return _mail;
	}
	public void setMail(String mail) {
		this._mail = mail;
	}
	private String	_name;
	private String	_mail;
	private Integer	_id;
}
